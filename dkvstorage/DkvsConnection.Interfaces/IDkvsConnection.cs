﻿using System.Net;
using DkvsCommandConnection.Interfaces;

namespace DkvsConnection.Interfaces
{
    public enum ConnectionState
    {
        Ready,
        Opened,
        Closed
    }
    /// <summary>
    /// Connection for using dkvStorage.
    /// </summary>
    public interface IDkvsConnection
    {
        IPEndPoint DbEndPoint { get; }
        ConnectionState State { get; }

        void Setup(string login, string password, IPEndPoint dbEndPoint);
        void Open();
        object ExecuteCommand(DkvsCommand command);
        void Close();
    }
}
