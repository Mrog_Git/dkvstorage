﻿using System;
using System.Net;
using System.Net.Sockets;
using DkvsCommandConnection.Interfaces;

namespace DkvsCommandConnection.Tcp
{
    public class CommandTcpSender: ICommandSender
    {
        private readonly TcpClient _client;
        private bool _used;

        public CommandTcpSender(IPEndPoint shardEndPoint)
        {
            _client = new TcpClient();
            _client.Connect(shardEndPoint);
            _used = false;
        }

        
        public void Send(DkvsCommand command)
        {
            if (!_used)
            {
                var ser = command.Serialize();
                Send(ser);
            }
            else throw new InvalidOperationException("Connection already was used.");
        }

        public void Send(byte[] command)
        {
            if (!_used)
            {
                var stream = _client.GetStream();
                byte[] buffer = new byte[command.Length + 4];
                Array.Copy(BitConverter.GetBytes(command.Length), buffer, 4);
                Array.Copy(command, 0, buffer, 4, command.Length);
                stream.Write(buffer, 0, buffer.Length);
            }
            else throw new InvalidOperationException("Connection already was used.");
        }

        public byte[] GetResult()
        {
            var stream = _client.GetStream();
            byte[] size = new byte[4];
            stream.Read(size, 0, 4);
            byte[] response = new byte[BitConverter.ToInt32(size, 0)];
            stream.Read(response, 0, BitConverter.ToInt32(size, 0));

            _client.Close();
            _used = true;
            return response;
        }
    }
}
