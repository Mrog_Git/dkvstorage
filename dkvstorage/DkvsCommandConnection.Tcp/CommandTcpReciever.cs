﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using DkvsCommandConnection.Interfaces;

namespace DkvsCommandConnection.Tcp
{
    public class CommandTcpReciever: ICommandReciever
    {
        private TcpListener _listener;
        private readonly IDictionary<int, byte[]> _storage;

        public List<Task> ClientSessions { get; }

        public CommandTcpReciever(IDictionary<int,byte[]> storage)
        {
            _storage = storage;
            ClientSessions = new List<Task>();
        }

        public void Listen(int port)
        {
            _listener?.Stop();
            _listener = TcpListener.Create(port);
            _listener.Start();
        }

        public void Close()
        {
            _listener.Stop();
        }

        public async Task RecieveConnectionAsync()
        {
            ClientSessions.Add(ExecuteAsync(await _listener.AcceptTcpClientAsync()));
        }

        public async Task ExecuteAsync(object client)
        {
            Console.WriteLine($"Recieved connection from ip = {(((TcpClient)client).Client.RemoteEndPoint as IPEndPoint)?.Address.ToString()}");
            NetworkStream stream = ((TcpClient)client).GetStream();

            byte[] size = new byte[4];
            await stream.ReadAsync(size, 0, 4);
            byte[] request = new byte[BitConverter.ToInt32(size, 0)];
            await stream.ReadAsync(request, 0, BitConverter.ToInt32(size, 0));


            DkvsCommand command = DkvsCommand.Deserialize(request);
            switch (command.CommandType)
            {
                case DkvsCommandType.Read:
                    await ExecuteRead(command.Key, client);
                    break;
                case DkvsCommandType.Update:
                    await ExecuteUpdate(command.Key, command.Value, client);
                    break;
                case DkvsCommandType.Store:
                    await ExecuteAdd(command.Key, command.Value, client);
                    break;
                case DkvsCommandType.Delete:
                    await ExecuteDelete(command.Key, client);
                    break;
            }

            ((TcpClient)client).Close();
        }

        public async Task ExecuteAdd(int key, byte[] value, object client)
        {
            byte[] response;
            try
            {
                _storage.Add(key, value);
                Console.WriteLine($"Added key = {key} value = {Encoding.UTF8.GetString(value)}");
                response = BitConverter.GetBytes(true);
            }
            catch
            {
                Console.WriteLine($"Adding fault key = {key} value = {Encoding.UTF8.GetString(value)}");
                response = BitConverter.GetBytes(false);
            }
            
            var stream = ((TcpClient) client).GetStream();
            byte[] buffer = new byte[response.Length + 4];
            Array.Copy(BitConverter.GetBytes(response.Length), buffer, 4);
            Array.Copy(response, 0, buffer, 4, response.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);

        }

        public async Task ExecuteRead(int key, object client)
        {
            byte[] response;
            try
            {
                _storage.TryGetValue(key, out response);
                Console.WriteLine($"Read key = {key}");
                if (response == null) response = new byte[0];
            }
            catch
            {
                Console.WriteLine($"Reading fault key = {key}");
                response = new byte[0];
            }

            var stream = ((TcpClient)client).GetStream();
            byte[] buffer = new byte[response.Length + 4];
            Array.Copy(BitConverter.GetBytes(response.Length), buffer, 4);
            Array.Copy(response, 0, buffer, 4, response.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);
            
        }

        public async Task ExecuteDelete(int key, object client)
        {
            byte[] response;
            try
            {
                response = BitConverter.GetBytes(_storage.Remove(key));
                Console.WriteLine($"Delete key = {key}");
            }
            catch
            {
                Console.WriteLine($"Deleting fault key = {key}");
                response = BitConverter.GetBytes(false);
            }

            var stream = ((TcpClient)client).GetStream();
            byte[] buffer = new byte[response.Length + 4];
            Array.Copy(BitConverter.GetBytes(response.Length), buffer, 4);
            Array.Copy(response, 0, buffer, 4, response.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);
            
        }

        public async Task ExecuteUpdate(int key, byte[] value, object client)
        {
            byte[] response;
            try
            {
                if (_storage.ContainsKey(key))
                {
                    var fl = _storage.Remove(key);
                    response = BitConverter.GetBytes(fl);
                    if (fl) _storage.Add(key,value);
                }
                else response = BitConverter.GetBytes(false);
                Console.WriteLine($"Updated key = {key} value = {Encoding.UTF8.GetString(value)}");
            }
            catch
            {
                Console.WriteLine($"Updating fault key = {key} value = {Encoding.UTF8.GetString(value)}");
                response = BitConverter.GetBytes(false);
            }

            var stream = ((TcpClient)client).GetStream();
            byte[] buffer = new byte[response.Length + 4];
            Array.Copy(BitConverter.GetBytes(response.Length), buffer, 4);
            Array.Copy(response, 0, buffer, 4, response.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);
            
        }
    }
}
