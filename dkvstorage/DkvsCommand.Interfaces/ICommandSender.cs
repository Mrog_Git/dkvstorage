﻿namespace DkvsCommandConnection.Interfaces
{
    public interface ICommandSender
    {
        void Send(byte[] command);
        void Send(DkvsCommand command);
        byte[] GetResult();

    }
}
