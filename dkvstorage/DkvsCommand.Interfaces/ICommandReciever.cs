﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DkvsCommandConnection.Interfaces
{
    public interface ICommandReciever
    {
        List<Task> ClientSessions { get; }

        void Listen(int port);
        Task RecieveConnectionAsync();
        Task ExecuteAsync(object client);
        Task ExecuteAdd(int key, byte[] value, object client);
        Task ExecuteRead(int key, object client);
        Task ExecuteDelete(int key, object client);
        Task ExecuteUpdate(int key, byte[] value, object client);
        void Close();
    }
}
