﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DkvsCommandConnection.Interfaces
{
    public enum DkvsCommandType
    {
        Read,
        Update,
        Store,
        Delete
    }

    [Serializable]
    public class DkvsCommand
    {
        public DkvsCommandType CommandType { get; private set; }
        public int Key { get; private set; }
        public byte[] Value { get; private set; }

        public DkvsCommand(DkvsCommandType commandType, int key)
        {
            CommandType = commandType;
            Key = key;
            Value = null;
        }

        public DkvsCommand(DkvsCommandType commandType, int key, byte[] value)
        {
            CommandType = commandType;
            Key = key;
            Value = value;
        }

        public static DkvsCommand Deserialize(byte[] byteArr)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(byteArr, 0, byteArr.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                DkvsCommand obj = (DkvsCommand)binForm.Deserialize(memStream);
                return obj;
            }
        }

        public byte[] Serialize()
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }
    }
}
