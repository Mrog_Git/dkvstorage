﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ClientKnowladgeConnector.Interfaces
{
    [Serializable]
    public class WorkRequest
    {
        public readonly int? SessionKey;
        public readonly int? Key;
        public readonly byte[] Command;
        public bool isReadCommand;

        public WorkRequest(int? sessionKey, int? key, byte[] command = null)
        {
            SessionKey = sessionKey;
            Key = key;
            Command = command;
        }
    }


    public enum CkcMessageType
    {
        /// <summary>
        /// Message type for getting session-key value.
        /// </summary>
        AuthorizationRequest,

        /// <summary>
        /// Message type for success-authorization response.
        /// </summary>
        AuthrizationSuccessResponse,

        /// <summary>
        /// Message type for fault-authorization response.
        /// </summary>
        AuthrizationFaultResponse,

        /// <summary>
        /// Message type for getting shard information for starage and receiving data.
        /// </summary>
        ShardRequest,

        /// <summary>
        /// Message with shard information. 
        /// </summary>
        ShardResponse,

        /// <summary>
        /// Message type for storage and receiving data.
        /// </summary>
        ExecutionRequest,

        /// <summary>
        /// Message with storage and receiving data result.
        /// </summary>
        ExecutionResponse,

        /// <summary>
        /// Message that indicates the connection is closing.
        /// </summary>
        CloseRequest
    }

    /// <summary>
    /// Message for communication between client and knowladge center.
    /// </summary>
    [Serializable]
    public class CkcMessage
    {
        public CkcMessage(CkcMessageType messageType, object content)
        {
            MessageType = messageType;
            Content = content;
        }

        public CkcMessageType MessageType { get; private set;}
        public object Content { get; private set; }

        public byte[] Serialize()
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        public static CkcMessage Deserialize(byte[] byteArr)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(byteArr, 0, byteArr.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                CkcMessage obj = binForm.Deserialize(memStream) as CkcMessage;
                return obj;
            }
        }
    }
}
