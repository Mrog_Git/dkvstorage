﻿using System.Net;
using System.Threading.Tasks;

namespace ClientKnowladgeConnector.Interfaces
{
    /// <summary>
    /// Client for connection with knowladge center.
    /// </summary>
    public interface ICkcClient
    {
        /// <summary>
        /// User session-key.
        /// </summary>
        int? SessionKey { get; }

        /// <summary>
        /// Data transmission type of connection.
        /// </summary>
        TransmissionType TransmissionType { get; }

        /// <summary>
        /// Type of sharder mechanism on knowladge center.
        /// </summary>
        SharderType SharderType { get; }

        /// <summary>
        /// Login and password of user.
        /// </summary>
        ClientInfo ClientInfo { get; set; }

        /// <summary>
        /// Creating connection between client and knowlage center. 
        /// </summary>
        /// <param name="ip">IP address of knowladge center.</param>
        /// <param name="port">Port of knowladge center.</param>
        void Connect(IPAddress ip, int port);

        /// <summary>
        /// Creating connection between client and knowlage center asynchronous. 
        /// </summary>
        /// <param name="ip">IP address of knowladge center.</param>
        /// <param name="port">Port of knowladge center.</param>
        Task ConnectAsync(IPAddress ip, int port);

        /// <summary>
        /// Closing connection with knowlage center.
        /// </summary>
        void Close();

        /// <summary>
        /// Authorizing and getting session key.
        /// </summary>
        void StartSession();

        /// <summary>
        /// Authorizing and getting session key asynchronous.
        /// </summary>
        Task StartSessionAsync();

        /// <summary>
        /// Asking knowladge center for selection a shard for storage and receiving data.
        /// </summary>
        /// <param name="isReadCommand">Is command read-type.</param>
        /// <param name="key">Key for sharder needs. Null by default</param>
        /// <returns>Endpoint of selected shard.</returns>
        IPEndPoint GetShardEndPoint(bool isReadCommand, int? key = null);

        /// <summary>
        /// Sending command to knowladge center forexecution on storage machine.
        /// </summary>
        /// <param name="command">Serialized command.</param>
        /// <param name="key">Key for sharder needs. Null by default</param>
        /// <returns>Result of executed command.</returns>
        byte[] SendForExecution(byte[] command, int? key = null);
    }
}
