﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ClientKnowladgeConnector.Interfaces
{
    /// <summary>
    /// Server side of connection between client and knowladge center.
    /// </summary>
    public interface ICkcServer
    {
        IDictionary<int, NetClientInfo> AuthorizedUsers { get; }
        List<Task> ClientSessions { get; }

        /// <summary>
        /// Starting listening for incoming connections.
        /// </summary>
        /// <param name="port">Port for listening.</param>
        void StartListen(int port);

        /// <summary>
        /// Closing connection.
        /// </summary>
        void Close();

        Task HandleConnectionAsync();
        Task HandleClientAsync(object client);
        Task AuthenticateAsync(CkcMessage request, object networkHandler);
        Task HandleWorkRequestAsync(CkcMessage request, object networkHandler);
        Task HandleExecutionRequestAsync(CkcMessage request, object networkHandler);

        bool Authorized(int sessonKey, EndPoint ep);
    }
}
