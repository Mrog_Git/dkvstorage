﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace ClientKnowladgeConnector.Interfaces
{
    public class NetClientInfo
    {
        public ClientInfo ClientInfo { get; set; }
        public EndPoint EndPoint { get; set; }

        public NetClientInfo(ClientInfo info, EndPoint ep)
        {
            ClientInfo = info;
            EndPoint = ep;
        }
    }

    [Serializable]
    public class ClientInfo
    {
        public string Login { get; }
        public byte[] Password { get; private set; }
        
        public ClientInfo(string login, string password)
        {
            Login = login;
            Password = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password));
        }
    }
}