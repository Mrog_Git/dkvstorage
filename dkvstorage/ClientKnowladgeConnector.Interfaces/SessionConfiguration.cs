﻿using System;

namespace ClientKnowladgeConnector.Interfaces
{
    /// <summary>
    /// Determines way of data transmission.
    /// </summary>
    public enum TransmissionType
    {
        ThroughKnowladgeCenter,
        DirectlyToShard,
        Null
    }

    /// <summary>
    /// Determines if sharder mechanism needs key for selecting a shard.
    /// </summary>
    public enum SharderType
    {
        NeedsKey,
        WithoutKey
    }

    [Serializable]
    public class SessionConfiguration
    {
        public readonly TransmissionType TransmissionType;
        public readonly SharderType SharderType;
        public readonly int SessionKey;

        public SessionConfiguration(TransmissionType transmissionType, int sessionKey, SharderType sharderType)
        {
            TransmissionType = transmissionType;
            SessionKey = sessionKey;
            SharderType = sharderType;
        }
    }
}
