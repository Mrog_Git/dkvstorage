﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DkvsCommandConnection.Interfaces;
using DkvsCommandConnection.Tcp;
using DkvsService.Interfaces;
using DkvsService.Tcp;

namespace DkvsStorageMachine
{
    static class DkvsStorageMachine
    {
        private static bool _closing;
        private static readonly ICommandReciever Reciever;
        private static readonly IServiceWorker ServiceWorker;

        static DkvsStorageMachine()
        {
            Reciever = new CommandTcpReciever(new Dictionary<int, byte[]>());   
            ServiceWorker = new ServiceWorkerTcp();
        }

        static void Main(string[] args)
        {
            _closing = false;

            try
            {
                var executeTask = Execute();
                var serviceTask = ServiceWork();

                Console.WriteLine("Press ESC for stop.");
                ConsoleKeyInfo input;
                do
                {
                    input = Console.ReadKey();
                } while (input.Key != ConsoleKey.Escape);


                ConsoleKey forceQuitResponse;
                do
                {
                    Console.Write("Application will close after all client sessions end. Force closing? (Y/N)");
                    forceQuitResponse = Console.ReadKey(false).Key;
                    Console.WriteLine();

                } while (forceQuitResponse != ConsoleKey.Y && forceQuitResponse != ConsoleKey.N);


                _closing = true;

                try
                {
                    if (forceQuitResponse == ConsoleKey.N)
                    {
                        executeTask.Wait(5000);
                        serviceTask.Wait(5000);
                        Task.WaitAll(Reciever.ClientSessions.ToArray());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Client handling error: " + e.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Application error: " + e.Message);
                Console.WriteLine("Press any key...");
                Console.ReadKey(true);
            }
            finally
            {
                Reciever.Close();
                ServiceWorker.Close();
            }
        }

        private static async Task Execute()
        {
            Reciever.Listen(15002);
            
            while (!_closing)
            {
                try
                {
                    await Reciever.RecieveConnectionAsync();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Connection handling error: " + e.Message);
                }

            }

        }

        private static async Task ServiceWork()
        {
            ServiceWorker.Listen(15003);

            while (!_closing)
            {
                try
                {
                    await ServiceWorker.HandleServiceRequests();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Service reqest handling error: " + e.Message);
                }
            }
        }
    }
}
