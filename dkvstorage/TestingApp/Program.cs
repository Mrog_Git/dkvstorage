﻿using System;
using System.Net;
using DistributedKeyValueStorage;

namespace TestingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var storage = new DkvStorage();
            try
            {
                Console.ReadLine();
                storage.Connect(IPAddress.Parse("127.0.0.1"), 15000, "root", "root");
                var res = storage.Store(1, BitConverter.GetBytes(7));
                Console.WriteLine(res.ToString());
                Console.ReadLine();
                var res3 = storage.Update(1, BitConverter.GetBytes(8));
                Console.WriteLine(res3.ToString());
                Console.ReadLine();
                var res4 = storage.Delete(1);
                Console.WriteLine(res4.ToString());
                Console.ReadLine();

                byte[] buffer;
                var res2 = storage.TryGetValue(1, out buffer);
                Console.WriteLine(res2.ToString());
                if (res2)Console.WriteLine(BitConverter.ToInt32(buffer,0));
                Console.ReadLine();

            }
            finally
            {
                storage.Close();
            }
        }
    }
}
