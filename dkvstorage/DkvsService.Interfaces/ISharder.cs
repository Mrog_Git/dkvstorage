﻿using System.Collections.Generic;
using System.Net;
using ClientKnowladgeConnector.Interfaces;

namespace DkvsService.Interfaces
{
    public interface ISharder
    {
        IList<IPEndPoint> Shards { get; }
        IDictionary<int, NetClientInfo> Users { get; set; }

        bool NeedsKey();
        IPEndPoint SelectShard(int sessionKey, int? key = null);
    }
}
