﻿using System.Collections.Generic;
using System.Net;

namespace DkvsService.Interfaces
{
    public interface IServiceManager
    {
        IList<IPEndPoint> Workers { get; }

        bool IsWorkerAlive(IPEndPoint workerEndPoint);
    }
}
