﻿namespace DkvsService.Interfaces
{
    public interface IUsersRegistry
    {
        void Register(string login, string password);

        void Delete(string login, string password);

        bool UserExists(string login, byte[] password);

        void Save();
    }
}
