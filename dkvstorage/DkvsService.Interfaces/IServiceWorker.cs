﻿using System.Threading.Tasks;

namespace DkvsService.Interfaces
{
    public interface IServiceWorker
    {
        void Listen(int port);
        Task HandleServiceRequests();
        Task ImAlive(object manager);
        void Close();
    }
}
