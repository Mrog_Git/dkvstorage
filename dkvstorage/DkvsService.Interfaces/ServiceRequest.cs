﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DkvsService.Interfaces
{
    public enum ServiceRequestType
    {
        Alive
    }

    [Serializable]
    public class ServiceRequest
    {
        public ServiceRequestType RequestType { get; set; }

        public ServiceRequest(ServiceRequestType type)
        {
            RequestType = type;
        }

        public byte[] Serialize()
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        public static ServiceRequest Deserialize(byte[] byteArr)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(byteArr, 0, byteArr.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                ServiceRequest obj = binForm.Deserialize(memStream) as ServiceRequest;
                return obj;
            }
        }
    }
}
