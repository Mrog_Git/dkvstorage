﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using ClientKnowladgeConnector.Interfaces;
using DkvsService.Interfaces;

namespace KnowladgeCenter
{
    public class SHAHashSharder: ISharder
    {
        public IList<IPEndPoint> Shards { get; }
        public IDictionary<int, NetClientInfo> Users { get; set; }

        public SHAHashSharder(IList<IPEndPoint> shards)
        {
            Shards = shards;
        }

        public IPEndPoint SelectShard(int sessionKey, int? key)
        {
            if (!Shards.Any() || key == null) return null;
            var hash = SHA256.Create().ComputeHash(BitConverter.GetBytes((int)key));
            return Shards[BitConverter.ToInt32(hash, 0) % Shards.Count];
        }

        public bool NeedsKey()
        {
            return true;
        }

    }
}
