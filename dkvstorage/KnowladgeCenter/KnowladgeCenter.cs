﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ClientKnowladgeConnector.Interfaces;
using ClientKnowladgeConnector.Tcp;
using DkvsService.Interfaces;
using DkvsService.Tcp;

namespace KnowladgeCenter
{
    class KnowladgeCenter
    {
        private static readonly IUsersRegistry Users;
        private static readonly ICkcServer Listener;
        private static readonly IServiceManager Manager;
        private static readonly Configuration Configuration;
        private static readonly bool ConfigError;
        private static bool _closing;

        static KnowladgeCenter()
        {
            ConfigError = false;
            try
            {
                Configuration = new Configuration();
                Users = new UsersRegistry();
            }
            catch
            {
                Console.WriteLine("Error while reading configuration files. Impossible to start application. \nPress any key...");
                Console.ReadKey(true);
                ConfigError = true;
                return;
            }
            Manager = new ServiceManagerTcp(Configuration.Shards);
            CheckShardsAlive();
            var sharder = Configuration.SharderType == SharderType.SHAHashSharder
                ? new SHAHashSharder(Configuration.Shards)
                : (ISharder)new LoginBasedSharder(Configuration.Shards);
            Listener = new CkcTcpServer(Users, sharder, Configuration.TransmissionType, Manager);
        }

        static void CheckShardsAlive()
        {
            foreach (IPEndPoint shard in Manager.Workers.ToList())
            {
                Console.Write($"Is shard (IP: {shard.Address.ToString()}) alive? - ");
                var alive = Manager.IsWorkerAlive(new IPEndPoint(shard.Address, shard.Port + 1));
                if (!alive)
                {
                    Console.WriteLine("No");
                    Manager.Workers.Remove(shard);
                }
                else Console.WriteLine("Yes");
            }
        }

        static void PrintHelp()
        {
            Console.WriteLine("'addusr' - register new user.");
            Console.WriteLine("'delusr' - delete user.");
        }

        static void RegisterUser()
        {
            try
            {
                Console.Write("Login: ");
                var login = Console.ReadLine();
                Console.Write("Password: ");
                var pwd = Console.ReadLine();
                Users.Register(login, pwd);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        static void DeleteUser()
        {
            try
            {
                Console.Write("Login: ");
                var login = Console.ReadLine();
                Console.Write("Password: ");
                var pwd = Console.ReadLine();
                Users.Delete(login, pwd);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        static void Main(string[] args)
        {
            if (ConfigError) return;

            try
            {
                var workTask = Work();

                string response;
                Console.WriteLine("Type 'quit' to stop or 'help' for more info.");
                do
                {
                    Console.Write("> ");
                    response = Console.ReadLine();
                    if (response == "help") PrintHelp();
                    else if (response == "addusr") RegisterUser();
                    else if (response == "delusr") DeleteUser();
                    else if (response != "quit")
                        Console.WriteLine("Incorrect command.");

                } while (response != "quit");

                ConsoleKey forceQuitResponse;
                do
                {
                    Console.Write("Application will close after all client sessions end. Force closing? (Y/N)");
                    forceQuitResponse = Console.ReadKey(false).Key;
                    Console.WriteLine();

                } while (forceQuitResponse != ConsoleKey.Y && forceQuitResponse != ConsoleKey.N);


                _closing = true;

                try
                {
                    if (forceQuitResponse == ConsoleKey.N)
                    {
                        workTask.Wait(5000);
                        Task.WaitAll(Listener.ClientSessions.ToArray());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Client handling error: " + e.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Application error: " + e.Message);
                Console.WriteLine("Press any key...");
                Console.ReadKey(true);
            }
            finally
            {
                Listener.Close();
                Users.Save();
                Configuration.SaveConfiguration();
            }
        }

        private static async Task Work()
        {
            Listener.StartListen(Configuration.ClientConnectionPort);

            while (!_closing)
            {
                try
                {
                    await Listener.HandleConnectionAsync();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Connection handling error: " + e.Message);
                }

            }

        }

    }
}
