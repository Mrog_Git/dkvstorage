﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml.Linq;
using ClientKnowladgeConnector.Interfaces;

namespace KnowladgeCenter
{
    public enum SharderType
    {
        SHAHashSharder,
        LoginBasedSharder,
        Null
    }

    public class Configuration
    {
        public TransmissionType TransmissionType { get; set; }
        public SharderType SharderType { get; set; }
        public IList<IPEndPoint> Shards { get; set; }
        public int ClientConnectionPort { get; set; }

        public Configuration()
        {
            var doc = XDocument.Load("config.xml");
            var config = doc.Element("Configurations");
            if (config == null) throw new Exception();
            var clientConnectionConfig = config.Element(nameof(ClientConnectionPort));
            var sharderTypeConfig = config.Element(nameof(SharderType));
            var transmissionTypeConfig = config.Element(nameof(TransmissionType));
            var shardsConfig = config.Element("Shards");

            if (clientConnectionConfig == null || sharderTypeConfig == null || transmissionTypeConfig == null ||
                shardsConfig == null) throw new Exception();

            ClientConnectionPort = Convert.ToInt32(clientConnectionConfig.Value);
            SharderType =
                    sharderTypeConfig.Value == nameof(SharderType.LoginBasedSharder)
                        ? SharderType.LoginBasedSharder
                        : (sharderTypeConfig.Value == nameof(SharderType.SHAHashSharder)
                            ? SharderType.SHAHashSharder
                            : SharderType.Null);
            if (SharderType == SharderType.Null) throw new Exception();

            TransmissionType =
                    transmissionTypeConfig.Value == nameof(TransmissionType.ThroughKnowladgeCenter)
                        ? TransmissionType.ThroughKnowladgeCenter
                        : (transmissionTypeConfig.Value == nameof(TransmissionType.DirectlyToShard)
                            ? TransmissionType.DirectlyToShard
                            : TransmissionType.Null);
            if (TransmissionType == TransmissionType.Null) throw new Exception();

            Shards = new List<IPEndPoint>();

            foreach (var shardConfig in shardsConfig.Elements("Shard"))
            {
                var ip = shardConfig.Attribute("Ip")?.Value;
                var port = shardConfig.Attribute("Port")?.Value;
                if (ip == null || port == null) throw new Exception();
                Shards.Add(new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(port)));
            }
        }

        public void SaveConfiguration()
        {
            var doc = new XDocument();
            var config = new XElement("Configurations");
            config.Add(new XElement(nameof(ClientConnectionPort))
            {
                Value = ClientConnectionPort.ToString()
            });
            config.Add(new XElement(nameof(SharderType))
            {
                Value =
                    SharderType == SharderType.LoginBasedSharder
                        ? nameof(SharderType.LoginBasedSharder)
                        : nameof(SharderType.SHAHashSharder)
            });
            config.Add(new XElement(nameof(TransmissionType))
            {
                Value =
                    TransmissionType == TransmissionType.DirectlyToShard
                        ? nameof(TransmissionType.DirectlyToShard)
                        : nameof(TransmissionType.ThroughKnowladgeCenter)
            });
            var shards = new XElement("Shards");
            foreach (var point in Shards)
            {
                var shard = new XElement("Shard");
                shard.Add(new XAttribute("Ip", point.Address.ToString()));
                shard.Add(new XAttribute("Port", point.Port.ToString()));
                shards.Add(shard);
            }
            config.Add(shards);
            doc.Add(config);
            doc.Save("config.xml");
        }
    }

}
