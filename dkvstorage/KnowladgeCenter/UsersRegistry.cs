﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using DkvsService.Interfaces;

namespace KnowladgeCenter
{
    public class UsersRegistry: IUsersRegistry
    {
        private readonly IDictionary<string, string> _users;

        public UsersRegistry()
        {
            _users = new Dictionary<string, string>();
            using (var stream = new FileStream("users.save", FileMode.OpenOrCreate))
            {
                var binForm = new BinaryFormatter();
                _users = (Dictionary<string,string>)binForm.Deserialize(stream);
            }
        }

        public void Save()
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var stream = new FileStream("users.save", FileMode.Create))
            {
                bf.Serialize(stream,_users);
            }
        }

        public bool UserExists(string login, byte[] password)
        {
            if (login == null) throw new ArgumentNullException(nameof(login));
            if (password == null) throw new ArgumentNullException(nameof(password));

            if (!_users.ContainsKey(login)) return false;
            if (_users[login] != Encoding.UTF8.GetString(password)) return false;

            return true;
        }

        public void Register(string login, string password)
        {
            var pwdHash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password));
            _users.Add(login, Encoding.UTF8.GetString(pwdHash));
        }

        public void Delete(string login, string password)
        {
            var pwdHash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password));
            if (!_users.ContainsKey(login)) throw new Exception($@"There is no any user with {login} - login in registry.");
            if (_users[login] != Encoding.UTF8.GetString(pwdHash)) throw new Exception($@"Password of user {login} is wrong.");
            _users.Remove(login);
        }
    }
}
