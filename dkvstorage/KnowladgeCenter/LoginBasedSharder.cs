﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using ClientKnowladgeConnector.Interfaces;
using DkvsService.Interfaces;

namespace KnowladgeCenter
{
    public class LoginBasedSharder: ISharder
    {
        public IList<IPEndPoint> Shards { get; }
        public IDictionary<int, NetClientInfo> Users { get; set; }

        public LoginBasedSharder(IList<IPEndPoint> shards)
        {
            Shards = shards;
        }

        public bool NeedsKey()
        {
            return false;
        }

        public IPEndPoint SelectShard(int sessionKey, int? key = null)
        {
            if (!Shards.Any()) return null;
            var hash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(Users[sessionKey].ClientInfo.Login));
            return Shards[BitConverter.ToInt32(hash, 0) % Shards.Count];
        }
    }
}
