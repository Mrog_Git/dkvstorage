﻿using System;
using System.Net;
using ClientKnowladgeConnector.Interfaces;
using DkvsCommandConnection.Interfaces;
using DkvsCommandConnection.Tcp;
using DkvsConnection.Interfaces;

namespace DkvsConnection.Tcp
{ 
    public class DkvsConnection: IDkvsConnection
    {
        private readonly ICkcClient _knowladgeConnector;

        public ConnectionState State { get; private set; }
        public IPEndPoint DbEndPoint { get; private set; }
        
        public DkvsConnection(ICkcClient knowladgeConnector)
        {
            _knowladgeConnector = knowladgeConnector;
            State = ConnectionState.Closed;
        }

        public void Setup(string login, string password, IPEndPoint dbEndPoint)
        {
            _knowladgeConnector.ClientInfo = new ClientInfo(login, password);
            DbEndPoint = dbEndPoint;
            State = ConnectionState.Ready;
        }

        public void Open()
        {
            if (State != ConnectionState.Ready)
                throw new InvalidOperationException("Connection have to be setuped before openning.");
            try
            {
                _knowladgeConnector.Connect(DbEndPoint.Address, DbEndPoint.Port);
                _knowladgeConnector.StartSession();
            }
            catch
            {
                _knowladgeConnector.Close();
                throw;
            }
            State = ConnectionState.Opened;
        }

        public object ExecuteCommand(DkvsCommand command)
        {
            if (State != ConnectionState.Opened)
                throw new InvalidOperationException("Connection have to be opened before executing any command.");

            byte[] result;
            if (_knowladgeConnector.TransmissionType == TransmissionType.ThroughKnowladgeCenter)
                result = _knowladgeConnector.SendForExecution(command.Serialize(),
                    _knowladgeConnector.SharderType == SharderType.NeedsKey ? (int?)command.Key : null);
            else
            {
                var ep =
                    _knowladgeConnector.GetShardEndPoint(command.CommandType == DkvsCommandType.Read,
                        _knowladgeConnector.SharderType == SharderType.NeedsKey
                            ? (int?)command.Key
                            : null);
                if (ep == null) throw new Exception("No one server is alive.");
                var sender = new CommandTcpSender(ep);
                sender.Send(command);
                result = sender.GetResult();
            }

            try
            {
                switch (command.CommandType)
                {
                    case DkvsCommandType.Delete:
                        return BitConverter.ToBoolean(result, 0);

                    case DkvsCommandType.Read:
                        return result.Length == 0 ? null : result;

                    case DkvsCommandType.Store:
                        return BitConverter.ToBoolean(result, 0);

                    case DkvsCommandType.Update:
                        return BitConverter.ToBoolean(result, 0);

                    default:
                        return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public void Close()
        {
            _knowladgeConnector.Close();
        }

    }
}
