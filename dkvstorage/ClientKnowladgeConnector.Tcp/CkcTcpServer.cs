﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ClientKnowladgeConnector.Interfaces;
using DkvsCommandConnection.Tcp;
using DkvsService.Interfaces;

namespace ClientKnowladgeConnector.Tcp
{
    public class CkcTcpServer: ICkcServer
    {
        private TcpListener _listener;
        private readonly ISharder _sharder;
        private readonly IUsersRegistry _users;
        private readonly IServiceManager _manager;
        public IDictionary<int, NetClientInfo> AuthorizedUsers { get; }

        public List<Task> ClientSessions { get; }
        public TransmissionType TransmissionType { get; } 

        public CkcTcpServer(IUsersRegistry users, ISharder sharder, TransmissionType transmissionType, IServiceManager manager)
        {
            AuthorizedUsers = new Dictionary<int, NetClientInfo>();
            _users = users;
            _sharder = sharder;
            _manager = manager;
            _sharder.Users = AuthorizedUsers;
            TransmissionType = transmissionType;
            ClientSessions = new List<Task>();
        }

        private int GetNewSessionKey()
        {
            var random = new Random();
            var key = random.Next();
            while (AuthorizedUsers.ContainsKey(key)) key = random.Next();
            return key;
        }

        public void StartListen(int port)
        {
            _listener?.Stop();
            _listener = TcpListener.Create(port);
            _listener.Start();
        }

        public void Close()
        {
            _listener.Stop();
        }
        
        public async Task HandleConnectionAsync()
        {
            ClientSessions.Add(HandleClientAsync( await _listener.AcceptTcpClientAsync()));
        }

        public async Task HandleClientAsync(object client)
        {
            Console.WriteLine($"Recieved connection from ip = {(((TcpClient)client).Client.RemoteEndPoint as IPEndPoint)?.Address.ToString()}");
            NetworkStream stream = ((TcpClient)client).GetStream();
            bool closing = false;
            List<Task> tasks = new List<Task>();

            while (!closing)
            {
                byte[] size = new byte[4];
                stream.Read(size, 0, 4);
                byte[] response = new byte[BitConverter.ToInt32(size, 0)];
                stream.Read(response, 0, BitConverter.ToInt32(size, 0));

                var request = CkcMessage.Deserialize(response);
                if (request == null) continue;
                switch (request.MessageType)
                {
                    case CkcMessageType.AuthorizationRequest:
                        await AuthenticateAsync(request, (TcpClient) client);
                        break;
                    case CkcMessageType.ShardRequest:
                        await HandleWorkRequestAsync(request, (TcpClient) client);
                        break;
                    case CkcMessageType.ExecutionRequest:
                        await HandleExecutionRequestAsync(request, (TcpClient) client);
                        break;
                    case CkcMessageType.CloseRequest:
                        AuthorizedUsers.Remove((int) request.Content);
                        closing = true;
                        break;
                }
            }

            ((TcpClient)client).Close();
        }

        public async Task AuthenticateAsync(CkcMessage request, object networkHandler)
        {
            var client = (TcpClient) networkHandler;
            NetworkStream stream = client.GetStream();

            byte[] ckcResponse;
            var cli = (ClientInfo)request.Content;
            if (cli != null && _users.UserExists(cli.Login, cli.Password))
            {
                var key = GetNewSessionKey();
                AuthorizedUsers.Add(key, new NetClientInfo(cli, client.Client.RemoteEndPoint));
                var cfg = new SessionConfiguration(TransmissionType, key,
                        _sharder.NeedsKey() ? SharderType.NeedsKey : SharderType.WithoutKey);
                ckcResponse = new CkcMessage(CkcMessageType.AuthrizationSuccessResponse, cfg).Serialize();
            }
            else
                ckcResponse = new CkcMessage(CkcMessageType.AuthrizationFaultResponse, null).Serialize();

            byte[] buffer = new byte[ckcResponse.Length + 4];
            Array.Copy(BitConverter.GetBytes(ckcResponse.Length), buffer, 4);
            Array.Copy(ckcResponse, 0, buffer, 4, ckcResponse.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);
        }


        public async Task HandleWorkRequestAsync(CkcMessage request, object networkHandler)
        {
            var client = (TcpClient)networkHandler;
            Console.WriteLine($"Handling work request from ip = {(client.Client.RemoteEndPoint as IPEndPoint)?.Address.ToString()}");
            NetworkStream stream = client.GetStream();

            byte[] ckcResponse;
            var workRequest = (WorkRequest)request.Content;
            if (workRequest?.SessionKey != null
                && !(workRequest.Key == null & _sharder.NeedsKey())
                && Authorized((int)workRequest.SessionKey, client.Client.RemoteEndPoint))
            {
                bool alive;
                IPEndPoint shard;
                do
                {
                    alive = true;
                    shard = _sharder.NeedsKey()
                        ? _sharder.SelectShard((int) workRequest.SessionKey, (int) workRequest.Key)
                        : _sharder.SelectShard((int) workRequest.SessionKey);
                    if (shard != null) alive = _manager.IsWorkerAlive(new IPEndPoint(shard.Address, shard.Port + 1));
                    if (!alive) _manager.Workers.Remove(shard);
                } while (!alive);
                //getting ep of repl if isReadCommand
                ckcResponse = new CkcMessage(CkcMessageType.ShardResponse, shard).Serialize();
            }
            else
            {
                ckcResponse = new CkcMessage(CkcMessageType.ShardResponse, null).Serialize();
            }

            byte[] buffer = new byte[ckcResponse.Length + 4];
            Array.Copy(BitConverter.GetBytes(ckcResponse.Length), buffer, 4);
            Array.Copy(ckcResponse, 0, buffer, 4, ckcResponse.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);

        }

        public async Task HandleExecutionRequestAsync(CkcMessage request, object networkHandler)
        {
            var client = (TcpClient)networkHandler;
            NetworkStream stream = client.GetStream();

            byte[] ckcResponse;
            var workRequest = (WorkRequest)request.Content;
            if (workRequest?.SessionKey != null
                && !(workRequest.Key == null & _sharder.NeedsKey())
                && workRequest.Command != null
                && Authorized((int)workRequest.SessionKey, client.Client.RemoteEndPoint))
            {
                IPEndPoint shard = _sharder.NeedsKey()
                    ? _sharder.SelectShard((int)workRequest.SessionKey, workRequest.Key)
                    : _sharder.SelectShard((int)workRequest.SessionKey);

                var sender = new CommandTcpSender(shard);
                sender.Send(workRequest.Command);
                var result = sender.GetResult();

                ckcResponse = new CkcMessage(CkcMessageType.ExecutionResponse, result).Serialize();
            }
            else
            {
                ckcResponse = new CkcMessage(CkcMessageType.ExecutionResponse, null).Serialize();
            }

            byte[] buffer = new byte[ckcResponse.Length + 4];
            Array.Copy(BitConverter.GetBytes(ckcResponse.Length), buffer, 4);
            Array.Copy(ckcResponse, 0, buffer, 4, ckcResponse.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);

        }

        public bool Authorized(int sessonKey, EndPoint ep)
        {
            if (!AuthorizedUsers.ContainsKey(sessonKey)) return false;
            var info = AuthorizedUsers[sessonKey];
            if (info.EndPoint != ep) return false;
            return true;
        }

    }
}
