﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Threading.Tasks;
using ClientKnowladgeConnector.Interfaces;

namespace ClientKnowladgeConnector.Tcp
{
    public class CkcTcpClient: ICkcClient
    {
        private TcpClient _cli;

        public ClientInfo ClientInfo { get; set; }
        public int? SessionKey { get; private set; }
        public bool Connected { get; private set; }
        public TransmissionType TransmissionType { get; private set; }
        public SharderType SharderType { get; private set; }

        public CkcTcpClient()
        {
            Connected = false;
            TransmissionType = TransmissionType.DirectlyToShard;
            SharderType = SharderType.NeedsKey;
        }

        private static void SendMessage(CkcMessage message, NetworkStream stream)
        {
            var ser = message.Serialize();
            byte[] buffer = new byte[ser.Length + 4];
            Array.Copy(BitConverter.GetBytes(ser.Length), buffer, 4);
            Array.Copy(ser, 0, buffer, 4, ser.Length);
            stream.Write(buffer, 0, buffer.Length);
        }

        private static CkcMessage GetMessage(NetworkStream stream)
        {
            byte[] size = new byte[4];
            stream.Read(size, 0, 4);
            byte[] response = new byte[BitConverter.ToInt32(size, 0)];
            stream.Read(response, 0, BitConverter.ToInt32(size, 0));

            return CkcMessage.Deserialize(response);
        }

        private static async Task SendMessageAsync(CkcMessage message, NetworkStream stream)
        {
            var ser = message.Serialize();
            byte[] buffer = new byte[ser.Length + 4];
            Array.Copy(BitConverter.GetBytes(ser.Length), buffer, 4);
            Array.Copy(ser, 0, buffer, 4, ser.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);
        }

        private static async Task<CkcMessage> GetMessageAsync(NetworkStream stream)
        {
            byte[] size = new byte[4];
            await stream.ReadAsync(size, 0, 4);
            byte[] response = new byte[BitConverter.ToInt32(size, 0)];
            await stream.ReadAsync(response, 0, BitConverter.ToInt32(size, 0));

            return CkcMessage.Deserialize(response);
        }

        public void Connect(IPAddress ip, int port)
        {
            _cli = new TcpClient();
            _cli.Connect(ip, port);
            Connected = true;
        }

        public async Task ConnectAsync(IPAddress ip, int port)
        {
            _cli = new TcpClient();
            await _cli.ConnectAsync(ip, port);
            Connected = true;
        }

        public void Close()
        {
            if (!Connected)
                throw new InvalidOperationException("Impossible to end session without connection.");

            NetworkStream stream = _cli.GetStream();
            SendMessage(new CkcMessage(CkcMessageType.CloseRequest, SessionKey), stream);
            _cli.Close();
            Connected = false;
        }

        public void StartSession()
        {
            if (!Connected)
                throw new InvalidOperationException("Impossible to start session without connection.");

            NetworkStream stream = _cli.GetStream();

            SendMessage(new CkcMessage(CkcMessageType.AuthorizationRequest, ClientInfo),stream);

            var ckcResponse = GetMessage(stream);
            if (ckcResponse != null && ckcResponse.MessageType == CkcMessageType.AuthrizationSuccessResponse)
            {
                var cfg = (SessionConfiguration)ckcResponse.Content;
                SessionKey = cfg.SessionKey;
                TransmissionType = cfg.TransmissionType;
                SharderType = cfg.SharderType;
            }
            else if (ckcResponse != null)
            {
                throw new AuthenticationException("Authentication fault.");
            }
        }

        public async Task StartSessionAsync()
        {
            if (!Connected)
                throw new InvalidOperationException("Impossible to start session without connection.");

            NetworkStream stream = _cli.GetStream();
            await SendMessageAsync(new CkcMessage(CkcMessageType.AuthorizationRequest, ClientInfo), stream);

            var ckcResponse = await GetMessageAsync(stream);
            if (ckcResponse != null && ckcResponse.MessageType == CkcMessageType.AuthrizationSuccessResponse)
            {
                var cfg = (SessionConfiguration)ckcResponse.Content;
                SessionKey = cfg.SessionKey;
                TransmissionType = cfg.TransmissionType;
                SharderType = cfg.SharderType;
            }
            else if (ckcResponse != null)
            {
                throw new AuthenticationException("Authentication fault.");
            }
        }

        public IPEndPoint GetShardEndPoint(bool isReadCommand, int? key = null)
        {
            if (!Connected)
                throw new InvalidOperationException("Impossible to use session without connection.");

            if (SessionKey == null)
                throw new ArgumentNullException(nameof(SessionKey),"Needs session key for using storage.");

            if (SharderType == SharderType.NeedsKey && key == null)
                throw new ArgumentNullException(nameof(key),"Type of sharder mechanism needs key for selecting exact shard.");

            NetworkStream stream = _cli.GetStream();
            var workRequest = new WorkRequest(SessionKey,key);
            workRequest.isReadCommand = isReadCommand;
            SendMessage(new CkcMessage(CkcMessageType.ShardRequest, workRequest), stream);

            var ckcResponse = GetMessage(stream);
            if (ckcResponse != null && ckcResponse.MessageType == CkcMessageType.ShardResponse)
            {
                return (IPEndPoint)ckcResponse.Content;
            }
            return null;
        }

        public byte[] SendForExecution(byte[] command, int? key = null)
        {
            if (!Connected)
                throw new InvalidOperationException("Impossible to use session without connection.");

            if (SessionKey == null)
                throw new ArgumentNullException(nameof(SessionKey), "Needs session key for using storage.");

            if (SharderType == SharderType.NeedsKey && key == null)
                throw new ArgumentNullException(nameof(key), "Type of sharder mechanism needs key for selecting exact shard.");

            NetworkStream stream = _cli.GetStream();
            var workRequest = new WorkRequest(SessionKey, key, command);
            SendMessage(new CkcMessage(CkcMessageType.ExecutionRequest, workRequest), stream);

            var ckcResponse = GetMessage(stream);
            if (ckcResponse != null && ckcResponse.MessageType == CkcMessageType.ExecutionResponse)
            {
                return (byte[])ckcResponse.Content;
            }
            return null;
        } 
    }
}
