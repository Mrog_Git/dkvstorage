﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using DkvsService.Interfaces;

namespace DkvsService.Tcp
{
    public class ServiceManagerTcp: IServiceManager
    {
        public IList<IPEndPoint> Workers { get; }

        public ServiceManagerTcp(IList<IPEndPoint> workers)
        {
            Workers = workers;
        }

        private static async Task SendMessageAsync(ServiceRequest message, NetworkStream stream)
        {
            var ser = message.Serialize();
            byte[] buffer = new byte[ser.Length + 4];
            Array.Copy(BitConverter.GetBytes(ser.Length), buffer, 4);
            Array.Copy(ser, 0, buffer, 4, ser.Length);
            await stream.WriteAsync(buffer, 0, buffer.Length);
        }

        private static async Task<ServiceRequest> GetMessageAsync(NetworkStream stream)
        {
            byte[] size = new byte[4];
            await stream.ReadAsync(size, 0, 4);
            byte[] response = new byte[BitConverter.ToInt32(size, 0)];
            await stream.ReadAsync(response, 0, BitConverter.ToInt32(size, 0));

            return ServiceRequest.Deserialize(response);
        }

        public bool IsWorkerAlive(IPEndPoint workerEndPoint)
        {
            try
            {
                var client = new TcpClient();
                var connectTask = client.ConnectAsync(workerEndPoint.Address, workerEndPoint.Port);
                connectTask.Wait(10000);
                if (connectTask.Status != TaskStatus.RanToCompletion) return false;

                var message = new ServiceRequest(ServiceRequestType.Alive);
                var writeTask = SendMessageAsync(message, client.GetStream());
                writeTask.Wait(10000);
                if (writeTask.Status != TaskStatus.RanToCompletion) return false;

                var responseTask = GetMessageAsync(client.GetStream());
                responseTask.Wait(10000);
                if (responseTask.Status != TaskStatus.RanToCompletion) return false;
                var response = responseTask.Result;
                if (response.RequestType == ServiceRequestType.Alive) return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

    }
}
