﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using DkvsService.Interfaces;

namespace DkvsService.Tcp
{
    public class ServiceWorkerTcp: IServiceWorker
    {
        private TcpListener _listener;
        
        public void Listen(int port)
        {
            _listener?.Stop();
            _listener = TcpListener.Create(port);
            _listener.Start();
        }

        public async Task HandleServiceRequests()
        {
            var client = await _listener.AcceptTcpClientAsync();

            NetworkStream stream = client.GetStream();

            byte[] size = new byte[4];
            await stream.ReadAsync(size, 0, 4);
            byte[] request = new byte[BitConverter.ToInt32(size, 0)];
            await stream.ReadAsync(request, 0, BitConverter.ToInt32(size, 0));


            ServiceRequest serviceRequest = ServiceRequest.Deserialize(request);
            switch (serviceRequest.RequestType)
            {
                case ServiceRequestType.Alive:
                    await ImAlive(client);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            client.Close();
        }

        public async Task ImAlive(object manager)
        {
            Console.WriteLine("Checking is alive.");
            var response = new ServiceRequest(ServiceRequestType.Alive);
            var ser = response.Serialize();
            byte[] buffer = new byte[ser.Length + 4];
            Array.Copy(BitConverter.GetBytes(ser.Length), buffer, 4);
            Array.Copy(ser, 0, buffer, 4, ser.Length);
            await ((TcpClient)manager).GetStream().WriteAsync(buffer, 0, buffer.Length);
        }

        public void Close()
        {
            _listener.Stop();
        }
    }
}
