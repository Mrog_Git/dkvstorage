﻿using System;
using System.Net;
using ClientKnowladgeConnector.Tcp;
using DkvsCommandConnection.Interfaces;
using DkvsConnection.Interfaces;

namespace DistributedKeyValueStorage
{
    public class DkvStorage
    {
        private readonly IDkvsConnection _connection = new DkvsConnection.Tcp.DkvsConnection(new CkcTcpClient());
        
        public void Connect(IPAddress dbAddress, int port, string login, string password)
        {
            _connection.Setup(login, password, new IPEndPoint(dbAddress,port));
            _connection.Open();
        }

        public bool Store(int key, byte[] value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (_connection.State != ConnectionState.Opened)
                throw new InvalidOperationException("Impossible to use storage berofe connection.");
            return (bool)_connection.ExecuteCommand(new DkvsCommand(DkvsCommandType.Store, key, value));
        }

        public bool TryGetValue(int key, out byte[] value)
        {
            if (_connection.State != ConnectionState.Opened)
                throw new InvalidOperationException("Impossible to use storage berofe connection.");
            value = (byte[])_connection.ExecuteCommand(new DkvsCommand(DkvsCommandType.Read, key));
            return value != null;
        }

        public bool Update(int key, byte[] value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (_connection.State != ConnectionState.Opened)
                throw new InvalidOperationException("Impossible to use storage berofe connection.");
            return (bool) _connection.ExecuteCommand(new DkvsCommand(DkvsCommandType.Update, key, value));
        }

        public bool Delete(int key)
        {
            if (_connection.State != ConnectionState.Opened)
                throw new InvalidOperationException("Impossible to use storage berofe connection.");
            return (bool)_connection.ExecuteCommand(new DkvsCommand(DkvsCommandType.Delete, key));
        }

        public void Close()
        {
            if (_connection.State == ConnectionState.Opened) _connection.Close();
        }
    }
}
